using System.Text.Json;

namespace BetterShop.Controllers.Resources;

public record CreateProductResource(
    string Name,
    string Description, 
    decimal Price, 
    decimal TaxRate);