using BetterShop.Controllers.Resources;
using BetterShop.DAL;
using BetterShop.Model;
using Microsoft.AspNetCore.Mvc;

namespace BetterShop.Controllers;

[ApiController]
public class ProductsController : ControllerBase
{
    public ProductsController()
    {
        
    }

    [HttpPost("product")]
    public async Task<IActionResult> Create(CreateProductResource request)
    {
        var mapper = new Model.ProductMapper();
        var product = mapper.CreateProductResourceToProduct(request);
        
        product.Id = Guid.NewGuid();
        
        var repository = new Repository<Product>();
        await repository.Save(product);
        
        Response.Headers.Add("x-urn", product.Urn);
        
        return Created(new Uri($"/product/{product.Id}", UriKind.Relative), product);
    }

    [HttpGet("product/{id}")]
    public async Task<IActionResult> Get(Guid id)
    {
        var repository = new Repository<Product>();
        var product = await repository.Get(id);
        
        return Ok(product);
    }
}