using BetterShop.DAL;
using BetterShop.Model;
using Microsoft.AspNetCore.Mvc;

namespace BetterShop.Controllers;

[ApiController]
public class CartController : ControllerBase
{
    [HttpPost("cart")]
    public async Task<IActionResult> Create(Cart cart)
    {
        cart.Id = Guid.NewGuid();

        var repository = new Repository<Cart>();
        await repository.Save(cart);

        return Created(new Uri($"cart/{cart.Id}", UriKind.Relative), cart);
    }

    [HttpGet("cart/{id}")]
    public async Task<IActionResult> Get(Guid id)
    {
        var repository = new Repository<Cart>();
        var cart = await repository.Get(id);
        
        return Ok(cart);
    }
}