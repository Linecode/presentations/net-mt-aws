using BetterShop.DAL;
using BetterShop.Model;
using Microsoft.AspNetCore.Mvc;

namespace BetterShop.Controllers;

[ApiController]
public class OrderController : ControllerBase
{
    [HttpPost("order")]
    public async Task<IActionResult> Create(Order order)
    {
        order.Id = Guid.NewGuid();
        
        var orderRepository = new OrdersRepository();
        await orderRepository.Save(order);

        return Created(new Uri($"order/{order.Id}", UriKind.Relative), order);
    }

    [HttpGet("order/{id}")]
    public async Task<IActionResult> Get(Guid id)
    {
        var orderRepository = new OrdersRepository();
        var order = await orderRepository.Get(id);
        
        return Ok(order);
    }

    [HttpPost("order/{id}/pay")]
    public async Task<IActionResult> Pay(Guid id)
    {
        var orderRepository = new OrdersRepository();
        var order = await orderRepository.Get(id);

        order.State = Order.Status.Paid;

        await orderRepository.Update(order);
        
        return Ok(order);
    }
}