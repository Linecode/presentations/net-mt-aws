using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;

namespace BetterShop.DAL;

public class Repository<T> where T : IEntityModel
{
    public async Task Save(T entity)
    {
        var client = new AmazonDynamoDBClient(RegionEndpoint.EUWest1);
        var context = new DynamoDBContext(client);

        var entityMap = context.ToDocument(entity).ToAttributeMap();

        entityMap.Remove("Id");
        entityMap.Add("Id", new AttributeValue { S = entity.Urn.ToLower() });
        entityMap.Add("Type", new AttributeValue { S = entity.GetType().Name });

        await client.TransactWriteItemsAsync(new TransactWriteItemsRequest
        {
            TransactItems = new List<TransactWriteItem>
            {
                new()
                {
                    Put = new Put
                    {
                        TableName = "better-shop",
                        Item = entityMap
                    }
                }
            }
        });
    }

    public async Task<T> Get(Guid id)
    {
        var client = new AmazonDynamoDBClient(RegionEndpoint.EUWest1);
        var context = new DynamoDBContext(client);
        
        var urn = $"urn:{typeof(T).Name.ToLower()}:{id}";

        var getItemRequest = new GetItemRequest
        {
            TableName = "better-shop",
            Key = new Dictionary<string, AttributeValue>
            {
                { "Id", new AttributeValue { S = urn } },
                { "Type", new AttributeValue { S = typeof(T).Name } }
            }
        };
        
        var getItemResponse = await client.GetItemAsync(getItemRequest);
        var entity = context.FromDocument<T>(Document.FromAttributeMap(getItemResponse.Item));
        
        return entity;
    }
}

public interface IEntityModel
{
    Guid Id { get; }
    string Urn { get; }
}