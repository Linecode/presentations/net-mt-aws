using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;

namespace BetterShop.DAL.Converters;

public class EnumConverter<T> : IPropertyConverter
{
    public DynamoDBEntry ToEntry(object value)
    {
        return new Primitive
        {
            Value = value.ToString()
        };
    }

    public object FromEntry(DynamoDBEntry entry)
    {
        return Enum.Parse(typeof(T), entry.AsString());
    }
}