using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;

namespace BetterShop.DAL.Converters;

public class UrnConverter<T> : IPropertyConverter
{
    public DynamoDBEntry ToEntry(object value)
    {
        return new Primitive
        {
            Value = $"urn:{typeof(T).Name}:{value}"
        };
    }

    public object FromEntry(DynamoDBEntry entry)
    {
        return Guid.Parse(entry.AsString().Split(':').Last());
    }
}