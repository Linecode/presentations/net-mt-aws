using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using BetterShop.Model;

namespace BetterShop.DAL;

public class OrdersRepository : Repository<Order>
{
    public async Task Update(Order order)
    {
        var entity = new Document
        {
            ["Id"] = order.Urn,
            ["Type"] = "Order",
            ["CartUrn"] = order.CartUrn,
            ["OwnerId"] = order.OwnerId,
            ["State"] = order.State.ToString()
        };

        var config = new UpdateItemOperationConfig
        {
            ReturnValues = ReturnValues.AllNewAttributes,
        };

        var table = Table.LoadTable(new AmazonDynamoDBClient(), "better-shop");
        await table.UpdateItemAsync(entity, config);
    }
}