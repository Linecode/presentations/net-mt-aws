using System.Reflection;
using MassTransit;

var builder = WebApplication.CreateBuilder(args);

builder.Configuration.AddJsonFile("appsettings.secrets.json", optional: true, reloadOnChange: true);

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var awsConfig = builder.Configuration.GetSection("AWS");
builder.Services.AddMassTransit(config => {
    config.SetEndpointNameFormatter(new KebabCaseEndpointNameFormatter($"better-shop-", false));
    
    config.UsingAmazonSqs((context, cfg) => {
        cfg.Host(new Uri($"amazonsqs://{awsConfig["region"]}"),
            hostConfigurator => {
                hostConfigurator.Scope($"better-shop-", true);

                var accessKey = awsConfig["accessKey"];
                var secretKey = awsConfig["secretKey"];
                hostConfigurator.AccessKey(accessKey);
                hostConfigurator.SecretKey(secretKey);
            });
        cfg.ConfigureEndpoints(context);
    });
    
    config.SetInMemorySagaRepositoryProvider();
    
    var entryAssembly = Assembly.GetEntryAssembly();

    config.AddConsumers(entryAssembly);
    config.AddSagaStateMachines(entryAssembly);
    config.AddSagas(entryAssembly);
    config.AddActivities(entryAssembly);
});

var app = builder.Build();

// Configure the HTTP request pipeline.
app.UseSwagger();
app.UseSwaggerUI();

app.UseAuthorization();

app.MapControllers();

app.Run();
