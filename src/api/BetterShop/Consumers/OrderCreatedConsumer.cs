using BetterShop.Contracts;
using MassTransit;

namespace BetterShop.Consumers;

public class OrderCreatedConsumer : IConsumer<OrderCreated>
{
    private readonly ILogger<OrderCreatedConsumer> _logger;

    public OrderCreatedConsumer(ILogger<OrderCreatedConsumer> logger)
    {
        _logger = logger;
    }
    
    public Task Consume(ConsumeContext<OrderCreated> context)
    {
        _logger.LogInformation("Order URN: {OrderUrn}, send email", context.Message.OrderUrn);
        
        return Task.CompletedTask;
    }
}