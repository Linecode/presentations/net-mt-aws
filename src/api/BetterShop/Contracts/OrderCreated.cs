namespace BetterShop.Contracts;

// ReSharper disable once InconsistentNaming
public interface OrderCreated
{
    string OrderUrn { get; }
}
