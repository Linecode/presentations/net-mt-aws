using Amazon.DynamoDBv2.DataModel;
using BetterShop.DAL;
using BetterShop.DAL.Converters;

namespace BetterShop.Model;

public class Order : IEntityModel
{
    [DynamoDBHashKey(typeof(UrnConverter<Product>))]
    public Guid Id { get; set; }
    public string Urn => $"urn:order:{Id}";
    public Guid OwnerId { get; set; }
    public string OwnerUrn => $"urn:user:{OwnerId}";

    public string CartUrn { get; set; } = null!;

    [DynamoDBProperty(typeof(EnumConverter<Status>))]
    public Status State { get; set; } = Status.Created;

    public enum Status
    {
        Created = 0,
        Paid = 1,
        Shipped = 2,
        Delivered = 3,
    }
}