using Amazon.DynamoDBv2.DataModel;
using BetterShop.DAL;
using BetterShop.DAL.Converters;

namespace BetterShop.Model;

public class Product : IEntityModel
{
    [DynamoDBHashKey(typeof(UrnConverter<Product>))]
    public Guid Id { get; set; }
    public string Urn => $"urn:product:{Id}";
    public string Name { get; set; } = null!;
    public string Description { get; set; } = null!;
    public decimal Price { get; set; }
    public decimal TaxRate { get; set; }
}