using BetterShop.Controllers.Resources;
using Riok.Mapperly.Abstractions;

namespace BetterShop.Model;

[Mapper]
public partial class ProductMapper
{
    public partial Product CreateProductResourceToProduct(CreateProductResource createProductResource);
}