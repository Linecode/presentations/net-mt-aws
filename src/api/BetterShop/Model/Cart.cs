using Amazon.DynamoDBv2.DataModel;
using BetterShop.DAL;
using BetterShop.DAL.Converters;

namespace BetterShop.Model;

public class Cart : IEntityModel
{
    [DynamoDBHashKey(typeof(UrnConverter<Product>))]
    public Guid Id { get; set; }
    public string Urn => $"urn:cart:{Id}";
    public Guid OwnerId { get; set; }
    public string OwnerUrn => $"urn:user:{OwnerId}";
    
    [DynamoDBProperty]
    public List<string> ProductsUrns { get; set; } = new();
}