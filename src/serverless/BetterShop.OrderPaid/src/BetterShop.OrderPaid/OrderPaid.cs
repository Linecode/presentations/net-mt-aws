// ReSharper disable once CheckNamespace
namespace BetterShop.Contracts;

// ReSharper disable once InconsistentNaming
public interface OrderPaid
{
    string OrderUrn { get; }
}