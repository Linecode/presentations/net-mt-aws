using System.Text;
using Amazon.Lambda.Core;
using Amazon.Lambda.SQSEvents;
using MassTransit;
using MassTransit.Transports;
using Microsoft.Extensions.DependencyInjection;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace BetterShop.OrderPaid;

public class Function
{
    private ServiceProvider _provider;

    public Function()
    {
        var services = new ServiceCollection();
        services.AddMassTransit(x =>
        {
            x.UsingInMemory();
            x.AddConsumer<OrderPaidConsumer>();
            x.AddConfigureEndpointsCallback((_, cfg) =>
            {
                cfg.UseRawJsonSerializer();
            });
        });
        _provider = services.BuildServiceProvider(true);
    }
    
    public async Task FunctionHandler(SQSEvent input, ILambdaContext context)
    {
        using var cts = new CancellationTokenSource(context.RemainingTime);
        using var scope = _provider.CreateScope();

        var ep = scope.ServiceProvider.GetRequiredService<IReceiveEndpointDispatcher<OrderPaidConsumer>>();

        var headers = new Dictionary<string, object>();

        foreach (var record in input.Records)
        {
            foreach (var key in record!.Attributes!.Keys)
                headers[key] = record.Attributes[key];

            foreach (var key in record!.MessageAttributes!.Keys)
                headers[key] = record.MessageAttributes[key];

            var body = Encoding.UTF8.GetBytes(record.Body);

            await ep.Dispatch(body, headers, cts.Token);
        }
    }
}

public class OrderPaidConsumer : IConsumer<Contracts.OrderPaid>
{
    public Task Consume(ConsumeContext<Contracts.OrderPaid> context)
    {
        Console.WriteLine($"Order {context.Message.OrderUrn} was paid!");
        
        // Doing some stuff
        return Task.CompletedTask;
    }
}