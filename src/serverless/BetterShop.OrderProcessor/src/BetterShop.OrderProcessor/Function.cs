using System.Text.Json;
using System.Text.Json.Serialization;
using Amazon.Lambda.Core;
using Amazon.Lambda.DynamoDBEvents;
using BetterShop.Contracts;
using MassTransit;
using Microsoft.Extensions.DependencyInjection;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace BetterShop.OrderProcessor;

public class Function
{
    private ServiceProvider _provider;
    
    public Function()
    {
        var services = new ServiceCollection();
        
        services.AddMassTransit(config => {
            config.SetEndpointNameFormatter(new KebabCaseEndpointNameFormatter($"better-shop-", false));
            config.UsingAmazonSqs((context, cfg) => {
                cfg.Host(new Uri("amazonsqs://eu-west-1"),
                    hostConfigurator => {
                        hostConfigurator.Scope($"better-shop-", true);
                        
                        hostConfigurator.AccessKey(Environment.GetEnvironmentVariable("ACCESS_KEY"));
                        hostConfigurator.SecretKey(Environment.GetEnvironmentVariable("SECRET_KEY"));
                    });
                cfg.ConfigureEndpoints(context);
            });
        });
        _provider = services.BuildServiceProvider(true);
    }
    
    public async Task FunctionHandler(DynamoDBEvent input, ILambdaContext context)
    {
        using var scope = _provider.CreateScope();
        var publishEndpoint = scope.ServiceProvider.GetRequiredService<IPublishEndpoint>();

        context.Logger.LogLine($"Begin handling of dynamodb stream event. Document count {input.Records.Count}");
        
        foreach (var dynamodbStreamRecord in input.Records)
        {
            context.Logger.LogLine("New Image:");
            context.Logger.LogLine(JsonSerializer.Serialize(dynamodbStreamRecord.Dynamodb.NewImage));
            context.Logger.LogLine("Old Image:");
            context.Logger.LogLine(JsonSerializer.Serialize(dynamodbStreamRecord.Dynamodb.OldImage));
            
            var type = dynamodbStreamRecord.Dynamodb.NewImage["Type"].S ?? "";

            if (type.ToLower() != "order")
                continue;
            
            var id = dynamodbStreamRecord.Dynamodb.NewImage["Id"].S ?? "";
            var state = dynamodbStreamRecord.Dynamodb.NewImage["State"].S ?? "";
            
            context.Logger.LogLine($"Id: {id}, Type: {type}");
            
            switch (state)
            {
                case "Created": 
                    await publishEndpoint.Publish<OrderCreated>(new { OrderUrn = id  });
                    break;
                
                case "Paid":
                    await publishEndpoint.Publish<OrderPaid>(new { OrderUrn = id });
                    break;
                
                default:
                    Console.WriteLine($"Unknown state: {state}");
                    break;
            }
        }
    }
}